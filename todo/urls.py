# todo/urls.py

from django.urls import path
from .views import (
    home, 
    TodoListView, TodoIDListView, TodoIDTitleListView, 
    UnresolvedTodoIDTitleListView, ResolvedTodoIDTitleListView,
    TodoIDUserIDListView, ResolvedTodoIDUserIDListView,
    UnresolvedTodoIDUserIDListView
)

urlpatterns = [
    path('', home, name='home'),
    path('api/todo/', TodoListView.as_view(), name='todo-list'),
    path('api/todo/ids/', TodoIDListView.as_view(), name='todo-id-list'),
    path('api/todo/ids-titles/', TodoIDTitleListView.as_view(), name='todo-id-title-list'),
    path('api/todo/unresolved-ids-titles/', UnresolvedTodoIDTitleListView.as_view(), name='unresolved-todo-id-title-list'),
    path('api/todo/resolved-ids-titles/', ResolvedTodoIDTitleListView.as_view(), name='resolved-todo-id-title-list'),
    path('api/todo/ids-userids/', TodoIDUserIDListView.as_view(), name='todo-id-userid-list'),
    path('api/todo/resolved-ids-userids/', ResolvedTodoIDUserIDListView.as_view(), name='resolved-todo-id-userid-list'),
    path('api/todo/unresolved-ids-userids/', UnresolvedTodoIDUserIDListView.as_view(), name='unresolved-todo-id-userid-list'),
]
