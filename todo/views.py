from django.shortcuts import render
from rest_framework import generics
from .models import Todo
from .serializers import (
    TodoIDSerializer, TodoIDTitleSerializer, TodoUserIDSerializer,
    TodoSerializer
)

class TodoListView(generics.ListAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer

class TodoIDListView(generics.ListAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoIDSerializer

class TodoIDTitleListView(generics.ListAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoIDTitleSerializer

class UnresolvedTodoIDTitleListView(generics.ListAPIView):
    queryset = Todo.objects.filter(is_resolved=False)
    serializer_class = TodoIDTitleSerializer

class ResolvedTodoIDTitleListView(generics.ListAPIView):
    queryset = Todo.objects.filter(is_resolved=True)
    serializer_class = TodoIDTitleSerializer

class TodoIDUserIDListView(generics.ListAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoUserIDSerializer

class ResolvedTodoIDUserIDListView(generics.ListAPIView):
    queryset = Todo.objects.filter(is_resolved=True)
    serializer_class = TodoUserIDSerializer

class UnresolvedTodoIDUserIDListView(generics.ListAPIView):
    queryset = Todo.objects.filter(is_resolved=False)
    serializer_class = TodoUserIDSerializer

def home(request):
    return render(request, 'todo/base.html')
