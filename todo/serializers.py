# todo/serializers.py

from rest_framework import serializers
from .models import Todo

class TodoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Todo
        fields = ['id', 'title', 'user_id', 'is_resolved']

class TodoIDSerializer(serializers.ModelSerializer):
    class Meta:
        model = Todo
        fields = ['id']

class TodoIDTitleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Todo
        fields = ['id', 'title']

class TodoUserIDSerializer(serializers.ModelSerializer):
    class Meta:
        model = Todo
        fields = ['id', 'user_id']

