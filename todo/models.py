# todo/models.py

from django.db import models

class Todo(models.Model):
    title = models.CharField(max_length=255)
    user_id = models.IntegerField()
    is_resolved = models.BooleanField(default=False)

    def __str__(self):
        return self.title
