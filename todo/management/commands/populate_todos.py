# todo/management/commands/populate_todos.py

from django.core.management.base import BaseCommand
from todo.models import Todo

class Command(BaseCommand):
    help = 'Populates the database with initial todo data'

    def handle(self, *args, **options):
        todos = [
            {'title': 'Revisar correos', 'user_id': 1, 'is_resolved': False},
            {'title': 'Completar reporte', 'user_id': 1, 'is_resolved': False},
            {'title': 'Actualizar software', 'user_id': 2, 'is_resolved': True},
            {'title': 'Reunión con el equipo', 'user_id': 3, 'is_resolved': False},
            {'title': 'Llamar a soporte técnico', 'user_id': 2, 'is_resolved': True},
            {'title': 'Enviar propuesta', 'user_id': 3, 'is_resolved': False},
            {'title': 'Revisar documentos', 'user_id': 1, 'is_resolved': True},
            {'title': 'Planificar sprint', 'user_id': 2, 'is_resolved': False},
            {'title': 'Desplegar aplicación', 'user_id': 3, 'is_resolved': True},
            {'title': 'Escribir documentación', 'user_id': 1, 'is_resolved': False},
        ]

        for todo in todos:
            Todo.objects.create(**todo)
        
        self.stdout.write(self.style.SUCCESS('Successfully populated todos'))
